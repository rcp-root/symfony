up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear demo_project-clear docker-pull docker-build docker-up demo_project-init
test: demo_project-test
test-coverage: demo_project-test-coverage
test-unit: demo_project-test-unit
test-unit-coverage: demo_project-test-unit-coverage

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

demo_project-init: demo_project-composer-install demo_project-assets-install demo_project-oauth-keys demo_project-wait-db demo_project-migrations demo_project-fixtures demo_project-ready

demo_project-clear:
	docker run --rm -v ${PWD}/demo_project:/app --workdir=/app alpine rm -f .ready

demo_project-composer-install:
	docker-compose run --rm demo_project-php-cli composer install

demo_project-assets-install:
	docker-compose run --rm demo_project-node yarn install
	docker-compose run --rm demo_project-node npm rebuild node-sass

demo_project-oauth-keys:
	docker-compose run --rm demo_project-php-cli mkdir -p var/oauth
	docker-compose run --rm demo_project-php-cli openssl genrsa -out var/oauth/private.key 2048
	docker-compose run --rm demo_project-php-cli openssl rsa -in var/oauth/private.key -pubout -out var/oauth/public.key
	docker-compose run --rm demo_project-php-cli chmod 644 var/oauth/private.key var/oauth/public.key

demo_project-wait-db:
	until docker-compose exec -T demo_project-postgres pg_isready --timeout=0 --dbname=app ; do sleep 1 ; done

demo_project-migrations:
	docker-compose run --rm demo_project-php-cli php bin/console doctrine:migrations:migrate --no-interaction

demo_project-fixtures:
	#docker-compose run --rm demo_project-php-cli php bin/console doctrine:fixtures:load --no-interaction

demo_project-ready:
	docker run --rm -v ${PWD}/demo_project:/app --workdir=/app alpine touch .ready

demo_project-assets-dev:
	docker-compose run --rm demo_project-node npm run dev

demo_project-test:
	docker-compose run --rm demo_project-php-cli php bin/phpunit

demo_project-test-coverage:
	docker-compose run --rm demo_project-php-cli php bin/phpunit --coverage-clover var/clover.xml --coverage-html var/coverage

demo_project-test-unit:
	docker-compose run --rm demo_project-php-cli php bin/phpunit --testsuite=unit

demo_project-test-unit-coverage:
	docker-compose run --rm demo_project-php-cli php bin/phpunit --testsuite=unit --coverage-clover var/clover.xml --coverage-html var/coverage

build-production:
	docker build --pull --file=demo_project/docker/production/nginx.docker --tag ${REGISTRY_ADDRESS}/demo_project-nginx:${IMAGE_TAG} demo_project
	docker build --pull --file=demo_project/docker/production/php-fpm.docker --tag ${REGISTRY_ADDRESS}/demo_project-php-fpm:${IMAGE_TAG} demo_project
	docker build --pull --file=demo_project/docker/production/php-cli.docker --tag ${REGISTRY_ADDRESS}/demo_project-php-cli:${IMAGE_TAG} demo_project
	docker build --pull --file=demo_project/docker/production/postgres.docker --tag ${REGISTRY_ADDRESS}/demo_project-postgres:${IMAGE_TAG} demo_project
	#docker build --pull --file=demo_project/docker/production/redis.docker --tag ${REGISTRY_ADDRESS}/demo_project-redis:${IMAGE_TAG} demo_project
	#docker build --pull --file=centrifugo/docker/production/centrifugo.docker --tag ${REGISTRY_ADDRESS}/centrifugo:${IMAGE_TAG} centrifugo

push-production:
	docker push ${REGISTRY_ADDRESS}/demo_project-nginx:${IMAGE_TAG}
	docker push ${REGISTRY_ADDRESS}/demo_project-php-fpm:${IMAGE_TAG}
	docker push ${REGISTRY_ADDRESS}/demo_project-php-cli:${IMAGE_TAG}
	docker push ${REGISTRY_ADDRESS}/demo_project-postgres:${IMAGE_TAG}
	#docker push ${REGISTRY_ADDRESS}/demo_project-redis:${IMAGE_TAG}
	#docker push ${REGISTRY_ADDRESS}/centrifugo:${IMAGE_TAG}

deploy-production:
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'rm -rf docker-compose.yml .env'
	scp -o StrictHostKeyChecking=no -P ${PRODUCTION_PORT} docker-compose-production.yml ${PRODUCTION_HOST}:docker-compose.yml
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "REGISTRY_ADDRESS=${REGISTRY_ADDRESS}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "IMAGE_TAG=${IMAGE_TAG}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "demo_project_APP_SECRET=${demo_project_APP_SECRET}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "demo_project_DB_PASSWORD=${demo_project_DB_PASSWORD}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "demo_project_REDIS_PASSWORD=${demo_project_REDIS_PASSWORD}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "demo_project_MAILER_URL=${demo_project_MAILER_URL}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "demo_project_OAUTH_FACEBOOK_SECRET=${demo_project_OAUTH_FACEBOOK_SECRET}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "STORAGE_BASE_URL=${STORAGE_BASE_URL}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "STORAGE_FTP_HOST=${STORAGE_FTP_HOST}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "STORAGE_FTP_USERNAME=${STORAGE_FTP_USERNAME}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "STORAGE_FTP_PASSWORD=${STORAGE_FTP_PASSWORD}" >> .env'
	#ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "CENTRIFUGO_WS_HOST=${CENTRIFUGO_WS_HOST}" >> .env'
	#ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "CENTRIFUGO_API_KEY=${CENTRIFUGO_API_KEY}" >> .env'
	#ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "CENTRIFUGO_SECRET=${CENTRIFUGO_SECRET}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'docker-compose pull'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'docker-compose up --build -d'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'until docker-compose exec -T demo_project-postgres pg_isready --timeout=0 --dbname=app ; do sleep 1 ; done'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'docker-compose run --rm demo_project-php-cli php bin/console doctrine:migrations:migrate --no-interaction'
